﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASP.NET_Lab5.DTO;
using ASP.NET_Lab5.Models;
using ASP.NET_Lab5.Services;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ASP.NET_Lab5.Controllers
{
    [Produces("application/json")]
    [Route("api/Player")]
    public class PlayerController : Controller
    {
        readonly IPlayerService _playerService;
        private readonly IMapper _mapper;
        public PlayerController(IPlayerService playerService, IMapper mapper)
        {
            _playerService = playerService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Players()
        {
            return Ok(_playerService.GetAllPlayers());
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Player([FromRoute]int id)
        {
            return Ok(_playerService.GetPlayer(id));
        }

        [HttpPost]
        public IActionResult CreatePlayer([FromBody] PlayerDto playerDto)
        {
            Player player = _mapper.Map<Player>(playerDto);
            _playerService.CreatePlayer(player);
            return Ok();
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult RemovePlayer([FromRoute]int id)
        {
            _playerService.RemovePlayer(id);
            return Ok();
        }

        [HttpPut]
        public IActionResult UpdatePlayer([FromBody] PlayerDto playerDto)
        {
            Player player = _mapper.Map<Player>(playerDto);
            _playerService.UpdatePlayer(player);
            return Ok();
        }
    }
}