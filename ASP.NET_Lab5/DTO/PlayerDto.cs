﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET_Lab5.DTO
{
    public class PlayerDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
