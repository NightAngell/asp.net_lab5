﻿using ASP.NET_Lab5.DTO;
using ASP.NET_Lab5.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NET_Lab5
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<PlayerDto, Player>();
            CreateMap<Player, PlayerDto>();
        }
    }
}
