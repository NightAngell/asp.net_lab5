﻿using System.Collections.Generic;
using ASP.NET_Lab5.Models;

namespace ASP.NET_Lab5.Services
{
    public interface IPlayerService
    {
        void CreatePlayer(Player player);
        IEnumerable<Player> GetAllPlayers();
        Player GetPlayer(int id);
        void RemovePlayer(int id);
        void UpdatePlayer(Player player);
    }
}