﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASP.NET_Lab5.Models;

namespace ASP.NET_Lab5.Services
{
    public class SimplePlayerService : IPlayerService
    {
        readonly static List<Player> _players = new List<Player>();

        public IEnumerable<Player> GetAllPlayers()
        {
            createTestPlayersOnce();
            return _players;
        }

        public Player GetPlayer(int id)
        {
            var player = _players.Find(p => p.Id == id);
            if (player == null) throw new Exception("Player not exist");
            return player;
        }

        public void CreatePlayer(Player player)
        {
            _players.Add(player);
        }

        public void UpdatePlayer(Player player)
        {
            int index = _players.FindIndex(p => p.Id == player.Id);
            if (index == -1) throw new Exception("Player not exist");
            _players[index] = player;
        }

        public void RemovePlayer(int id)
        {
            _players.Remove(
                _players.Find(p => p.Id == id)
            );
        }

        private void createTestPlayersOnce()
        {
            if (_players.Count > 0) return;

            _players.Add(
                new Player
                {
                    Id = 1,
                    Name = "First"
                }
            );
            _players.Add(
                new Player
                {
                    Id = 2,
                    Name = "Second"
                }
            );
        }
    }
}
